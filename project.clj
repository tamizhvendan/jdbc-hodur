(defproject jdbc-hodur "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [hodur/engine "0.1.6"]
                 [inflections "0.13.2"]]
  :profiles {:test {:dependencies [[hodur/graphviz-schema "0.1.0"]
                                   [org.postgresql/postgresql "42.2.5"]
                                   [clojure.jdbc.metadata "0.1.0-SNAPSHOT"]]}})
