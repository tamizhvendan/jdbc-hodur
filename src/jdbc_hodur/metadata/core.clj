(ns jdbc-hodur.metadata.core
  (:require [inflections.core :as inf]))

(defn- sql-type [code]
  (.getName (java.sql.JDBCType/valueOf code)))

(defn default-schema [db-product-name]
  (case db-product-name
    "postgresql" "public"))

(defn hodur-type [jdbc-type]
  (let [date-time (symbol "DateTime")
        bool (symbol "Boolean")
        float (symbol "Float")
        integer (symbol "Integer")
        string (symbol "String")]
    (case (sql-type jdbc-type)
      ("INTEGER" "SMALLINT") integer
      ("TIMESTAMP" "DATE") date-time
      "NUMERIC" float
      "VARCHAR" string
      "BIT" bool
      ("CHAR" "BINARY" "DISTINCT" "ARRAY" "OTHER") string)))

(defn- hodur-keyword [ns attr]
  (keyword (str "jdbc.metadata." ns) attr))

(defn- hodur-column-metadata [& attrs]
  (inf/transform-keys (apply hash-map attrs) #(hodur-keyword "column" %)))

(defn- hodur-relation-metadata [& attrs]
  (inf/transform-keys (apply hash-map attrs) #(hodur-keyword "relation" %)))

(defn column [schema table-name {:keys [name default column-size jdbc-data-type
                                        is-auto-increment data-type]}]
  (hodur-column-metadata "data-type" data-type
                         "default" default
                         "name" name
                         "column-size" column-size
                         "is-auto-increment" is-auto-increment
                         "jdbc-data-type" jdbc-data-type
                         "table-schema" schema
                         "table-name" table-name))

(defn relation [{:keys [schema table-name type primary-key]}]
  (let [primary-keys (->> (:columns primary-key)
                          (sort-by :order)
                          (map :name)
                          seq)]
    (hodur-relation-metadata "schema" schema
                             "name" table-name
                             "type" type
                             "primary-keys" primary-keys)))

(defn one-to-any-relationship [cardinality [pk-schema pk-table pk-column-name] [fk-schema fk-table fk-column-name]]
  (let [relationship (case cardinality
                       :one :one-to-one
                       :many :one-to-many)]
    (hodur-column-metadata "table-schema" pk-schema
                           "table-name" pk-table
                           "name" pk-column-name
                           "relationship" relationship
                           "fk-table-schema" fk-schema
                           "fk-table-name" fk-table
                           "fk-column-name" fk-column-name)))

(defn many-to-many-relationship [[left-schema left-table left-column-name]
                                 [bridge-schema bridge-table bridge-left-column-name bridge-right-column-name]
                                 [right-schema right-table right-column-name]]
  (hodur-column-metadata "left-table-schema" left-schema
                         "left-table-name" left-table
                         "left-column-name" left-column-name
                         "relationship" :many-to-many
                         "bridge-table-schema" bridge-schema
                         "bridge-table-name" bridge-table
                         "bridge-left-column-name" bridge-left-column-name
                         "bridge-right-column-name" bridge-right-column-name
                         "right-table-schema" right-schema
                         "right-table-name" right-table
                         "right-column-name" right-column-name))

(comment
  (sql-type 5)
  (jdbc-type->hodur-type 5))