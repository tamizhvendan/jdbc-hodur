(ns jdbc-hodur.metadata.query
  (:require [datascript.core :as d]))

(defn pk-tables
  "Returns a lazy sequence of list of tables that has primary-keys"
  [hodur-schema]
  (->> (d/q '[:find ?type ?primary-keys ?column-name ?column-type
              :where
              [?t :jdbc-hodur/tag true]
              [?t :jdbc.metadata.relation/type :table]
              [?t :type/PascalCaseName ?type]
              [?t :jdbc.metadata.relation/name ?table-name]
              [?t :jdbc.metadata.relation/schema ?table-schema]
              [?t :jdbc.metadata.relation/primary-keys ?primary-keys]
              [?c :jdbc.metadata.column/table-name ?table-name]
              [?c :jdbc.metadata.column/table-schema ?table-schema]
              [?c :jdbc.metadata.column/name ?column-name]
              [?c :field/type ?field-type]
              [?c :jdbc.metadata.column/jdbc-data-type _]
              [?field-type :type/name ?column-type]] @hodur-schema)
       (group-by (fn [[tn pks]]
                   [tn pks]))
       (map (fn [[k v]]
              (let [[table-type primary-keys] k]
                {:type table-type
                 :primary-keys primary-keys
                 :columns (map (fn [[_ _ column-name column-type]]
                                 {:name column-name
                                  :type (keyword column-type)}) v)})))))

(defn columns
  "Returns column hodur schema of fields like [:Actor/firstName :Actor/lastName]"
  [hodur-schema fields]
  (let [relation-type (-> (first fields) namespace keyword)
        field-names (set (map #(keyword (name %)) fields))]
    (map first
         (d/q '[:find (pull ?c [*])
                :in $ ?relation-type ?field-names
                :where
                [?c :jdbc-hodur/tag true]
                [?r :jdbc-hodur/tag true]
                [?c :field/camelCaseName ?field-name]
                [?r :type/PascalCaseName ?relation-type]
                [?r :jdbc.metadata.relation/type :table]
                [?r :jdbc.metadata.relation/name ?table-name]
                [?r :jdbc.metadata.relation/schema ?table-schema]
                [?c :jdbc.metadata.column/table-name ?table-name]
                [?c :jdbc.metadata.column/table-schema ?table-schema]
                [(contains? ?field-names ?field-name)]] @hodur-schema relation-type field-names))))

(defn column
  "Returns column hodur schema of a field"
  [hodur-schema field]
  (first (columns hodur-schema [field])))