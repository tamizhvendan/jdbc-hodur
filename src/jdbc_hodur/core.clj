(ns jdbc-hodur.core
  (:require [inflections.core :as inf]
            [hodur-engine.core :as hodur]
            [clojure.string :as str]
            [jdbc-hodur.metadata.core :as metadata]))

(defn- remove-nil-values [m]
  (into {} (remove (comp nil? second) m)))

(defn- type-name [db-product-name schema table-name]
  (let [pascal-cased-table-name (inf/camel-case table-name)]
    (if (= (metadata/default-schema db-product-name) schema)
      (symbol pascal-cased-table-name)
      (-> (inf/camel-case schema)
          (str pascal-cased-table-name)
          symbol))))

(defn- any-to-many-field-name [db-product-name schema table-name]
  (if (= (metadata/default-schema db-product-name) schema)
    (-> (inf/plural table-name) inf/hyphenate symbol)
    (-> (str schema "-" table-name) inf/plural inf/hyphenate symbol)))

(defn- remove-id-suffix [name]
  (str/replace name #"_id$" ""))

(defn merge-hodur-type-maps [x y]
  {:fields (concat (:fields x) (:fields y))
   :remarks (or (:remarks x) (:remarks y))
   :relation-metadata (or (:relation-metadata x) (:relation-metadata y))})

(defn- bridge-table? [foreign-keys]
  (-> (map #(get-in % [:reference :table]) foreign-keys)
      distinct
      count
      (= 2)))

(defn- foreign-keys->m2m-hodur-type-map [db-product-name pk-schema pk-table foreign-keys]
  (when (bridge-table? foreign-keys)
    (let [[{left-bridge-col-name :column-name
            left-ref :reference}
           {right-bridge-col-name :column-name
            right-ref :reference}] foreign-keys
          {left-col-name :column-name
           left-table :table} left-ref
          {left-table-name :name
           left-table-schema :schema} left-table
          {right-col-name :column-name
           right-table :table} right-ref
          {right-table-name :name
           right-table-schema :schema} right-table
          left-field-name (any-to-many-field-name db-product-name right-table-schema right-table-name)
          right-field-name (any-to-many-field-name db-product-name left-table-schema left-table-name)
          left-hodur-metadata {:type (type-name db-product-name right-table-schema right-table-name)
                               :cardinality [0 'n]}
          right-hodur-metadata {:type (type-name db-product-name left-table-schema left-table-name)
                                :cardinality [0 'n]}
          left-ref-metadata (metadata/many-to-many-relationship [left-table-schema left-table-name left-col-name]
                                                                [pk-schema pk-table left-bridge-col-name right-bridge-col-name]
                                                                [right-table-schema right-table-name right-col-name])
          right-ref-metadata (metadata/many-to-many-relationship [right-table-schema right-table-name right-col-name]
                                                                 [pk-schema pk-table right-bridge-col-name left-bridge-col-name]
                                                                 [left-table-schema left-table-name left-col-name])]
      {[left-table-schema left-table-name] {:fields (list (with-meta left-field-name (merge left-ref-metadata left-hodur-metadata)))}
       [right-table-schema right-table-name] {:fields (list (with-meta right-field-name (merge right-ref-metadata right-hodur-metadata)))}})))

(defn- foreign-key->1tom-hodur-type-map [db-product-name pk-schema pk-table {:keys [column-name reference]}]
  (let [{fk-schema :schema
         fk-table :name} (:table reference)
        fk-column-name (:column-name reference)
        field-name (any-to-many-field-name db-product-name pk-schema pk-table)
        hodur-metadata {:type (type-name db-product-name pk-schema pk-table)
                        :cardinality [0 'n]}
        ref-metadata (metadata/one-to-any-relationship :many [fk-schema fk-table fk-column-name] [pk-schema pk-table column-name])]
    {[fk-schema fk-table] {:fields (list (with-meta field-name (merge ref-metadata hodur-metadata)))}}))

(defn- foreign-keys->1tom-hodur-type-map [db-product-name schema table-name foreign-keys]
  (apply merge-with
         merge-hodur-type-maps
         (map #(foreign-key->1tom-hodur-type-map db-product-name schema table-name %) foreign-keys)))

(defn- foreign-key->1to1-hodur-type-map [db-product-name pk-schema pk-table {:keys [column-name reference]}]
  (let [{fk-schema :schema
         fk-table :name} (:table reference)
        fk-column-name (:column-name reference)
        field-name (-> (remove-id-suffix column-name) inf/hyphenate symbol)
        hodur-metadata {:type (type-name db-product-name fk-schema fk-table)}
        fk-metadata (metadata/one-to-any-relationship :one
                                                      [pk-schema pk-table column-name]
                                                      [fk-schema fk-table fk-column-name])]
    {[pk-schema pk-table] {:fields (list (with-meta field-name (merge fk-metadata hodur-metadata)))}}))

(defn- foreign-keys->1to1-hodur-type-map [db-product-name schema table-name foreign-keys]
  (apply merge-with
         merge-hodur-type-maps
         (map #(foreign-key->1to1-hodur-type-map db-product-name schema table-name %) foreign-keys)))

(defn- column->hodur-type-map [schema table-name
                               {:keys [remarks is-nullable jdbc-data-type name]
                                :as column}]
  (let [column-metadata (metadata/column schema table-name column)
        hodur-metadata {:doc remarks
                        :optional is-nullable
                        :type (metadata/hodur-type jdbc-data-type)}
        field-name (symbol (inf/hyphenate name))]
    (with-meta field-name (remove-nil-values (merge column-metadata hodur-metadata)))))

(defn- columns->hodur-type-map [schema table-name columns]
  {[schema table-name] {:fields (map #(column->hodur-type-map schema table-name %) columns)}})

(defn- relation->hodur-type-map [db-product-name {:keys [schema table-name remarks
                                                         columns foreign-keys]
                                                  :as relation}]
  (merge-with merge-hodur-type-maps
              {[schema table-name] {:remarks remarks :relation-metadata (metadata/relation relation)}}
              (columns->hodur-type-map schema table-name columns)
              (foreign-keys->1to1-hodur-type-map db-product-name schema table-name foreign-keys)
              (foreign-keys->1tom-hodur-type-map db-product-name schema table-name foreign-keys)
              (foreign-keys->m2m-hodur-type-map db-product-name schema table-name foreign-keys)))

(defn- relations->hodur-type-map [db-product-name relations]
  (apply merge-with merge-hodur-type-maps
         (map #(relation->hodur-type-map db-product-name %) relations)))

(defn- hodur-type [db-product-name
                   {:keys [hodur]}
                   [[schema table-name] {:keys [fields remarks relation-metadata]}]]
  (let [hodur-metadata {:doc remarks
                        :jdbc-hodur/tag-recursive true}]
    (-> (type-name db-product-name schema table-name)
        (with-meta (remove-nil-values (merge relation-metadata (get-in hodur [:type :metadata]) hodur-metadata)))
        vector
        (conj (vec fields)))))

(defn hodur-schema [config jdbc-metadata]
  (let [db-product-name (get-in jdbc-metadata [:db :product-name])
        relations (:relations jdbc-metadata)
        hodur-type-map (relations->hodur-type-map db-product-name relations)]
    (-> (mapcat #(hodur-type db-product-name config %) hodur-type-map)
        vec
        hodur/init-schema)))

(comment
  (def db-spec {:dbtype "postgres"
                :dbname "dvdrental"
                :user "postgres"
                :password "postgres"})
  (def config {:hodur {:type {:metadata {:graphviz/tag-recursive true}}}})

  (require '[clojure.jdbc.metadata :as jdbc-metadata])
  (defn fetch-hodur-schema [config db-spec]
    (hodur-schema config (jdbc-metadata/fetch db-spec)))

  (require '[hodur-graphviz-schema.core :as hodur-graphviz])
  (defn graphviz-schema []
    (->> (fetch-hodur-schema config db-spec)
         hodur-graphviz/schema
         (spit "diagram.dot")))
  (graphviz-schema))